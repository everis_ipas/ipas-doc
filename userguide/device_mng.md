# Device Management

## What is a Device ?

**Device** in the Metric HOME environment is refered to any kind of "hardware device" but also "sofware instance". It depens on the related product and its metric definition.

You could manage your devices from the Device section in the Navigator Bar

![Management Bar](/images/home/HOME_NAVIGATOR_BAR_DEVICE_SELECTED_00.PNG)

Management devices includes

* [**New device**](#new-device) Add a new device for existing products
* [**Edit and update**](#edit-device) Edit an already deployed device to update the configuration on a product
* [**Delete device**](#delete-device) Delete a product in already deployed device

## Requeriments

In order to add a devices you should previously tested in your laboratory environtment you can configure this device/instance for your desired product and all is working fine.

---

## New device

> You need the main parameters for the device configuration depending on the selected engine. On the exmaple the selected engine is Telegraf

1. Click "New" on the device management view.

    ![New-Device](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_00.PNG)

1. Select if you want to create a device from another one or a new one

    ![New-Select_Type](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_01.PNG)

    Seelect existing product if you want to copy a configuration:

    ![New-Device_Properties](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_02.PNG)

1. Device properties

    Fill device properties according to:

    Field | Description | Normalization | Example
    --- | --- | --- | ---
    `Device name` | IP or FQDN of the device to deploy products on | <ul><li>Must not contain blank spaces</li></ul> | <ul><li>`127.0.0.1`</li><li>`localhost`</li></ul>
    

    ![New-Device_Properties](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_03.PNG)

1. Product Config (device parameters for this product): 

    The product config needs first to know what product you would like to deploy.
    Select it from the list, and click the ![Plus button](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_05.PNG) button.


    Field | Description | Normalization | Example
    --- | --- | --- | ---
    `Available products` | List of products you can deploy on | -- | <ul><li>`linux`</li><li>`tomcat`</li></ul>

    ![New-Product_Config](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_04.PNG)
 
    You will see a new view with a list with posible different configurations available on the selected product called `engines`. A short description on each config will appear if you hold the mouse over each option. Select your desired option and click the "plus" button.

    Field | Description | Normalization | Example
    --- | --- | --- | ---
    `Available configs` | List of configs available on selected engine | -- | <ul><li>`Telegraf Base Linux`</li><li>`Telegraf Base Windows`</li></ul>

    ![New-Config_Selector](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_06.PNG)

    No you will be prompted for a lot of parameters for this device related to this product and the selected configuration for this product and engine.

    > Note : the parameters will be different for each product/engine
    
    ![New-Config_Fill](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_07.PNG)

1. Environment Config (Service Config)

    Now you should select for a valid enviroment where to deploy this device with this product, the available environments are provided and enabled by the platform administrators.

    Could be more than one, and also could be from diferent engine type if Gather process needs for diferent gather engines.

    ![New-Env_Config](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_08.PNG)

1. Finish config and Send Job.

    Now come back to the first selector and click on the finish config ( or cance if you don't want to send), Click finish again.

    ![New-Fininsh](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_09.PNG)

    you will see a spin lock while waiting for the job begins the execution.

    ![New-Spinner](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_10.PNG)

    When the job has been sent you will be redirected to the device list, where you will see your device in a pending state. ( Waiting to finish )

    ![New-List](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_11.PNG)

    You will be able to see the state and the result if you click on the state 

    ![New-Result_Caret](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_12.PNG)

    ![New-Result_Modal](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_13.PNG)

---

## Edit device

> You need the main parameters for the device configuration depending on the selected engine. On the exmaple the selected engine is Telegraf

> When a user edit a device, the product and configuration can't be modified. If you want to change it, delete and create a new device

1. Search for the device you want to edit
    You can use the fiter input box to find your device. Remember that device can appear for each installed product.
    ![Edit-List](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_00.PNG)

1. Click in edit button
    The available actions appear on `Action` column and allows the user to interact wich each product/device deployment. To edit the deploy, click on ![Edit-Edit_Button](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_01.PNG) button

1. Product Config
    Go to each engine by tabs and review your configuration
    ![Edit-Product_Config](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_02.PNG)

1. Environment Config (Service Config)
    Go to Service Config to review and click on the deployed environmenets

    ![Edit-Env_Config](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_03.PNG)

1. Finish config and Send Job.

    Now come back to the first selector and click on the finish config ( or cance if you don't want to send), Click finish again.

    ![Edit-Finish](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_04.PNG)

    you will see a spin lock while waiting for the job begins the execution.

    ![Edit-Spinner](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_05.PNG)

    When the job has been sent you will be redirected to the device list, where you will see your device in a pending state. ( Waiting to finish )

    ![Edit-List](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_06.PNG)

    You will be able to see the state and the result if you click on the state 

    ![Edit-Result_Caret](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_07.PNG)

    ![Edit-Result_Modal](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_08.PNG)

---

## Delete device

> You need the main parameters for the device configuration depending on the selected engine. On the exmaple the selected engine is Telegraf

> When a user deletes a device, the product and configuration can't be modified. If you want to change it, delete and create a new device. 

1. Search for the device you want to delete
    You can use the fiter input box to find your device. Remember that device can appear for each installed product.
    ![Delete-List](/images/home/HOME_DEVICE_MNG_DELETE_DEVICE_00.PNG)

1. Click in delete button
    The available actions appear on `Action` column and allows the user to interact wich each product/device deployment. To edit the deploy, click on     ![Delete-Delete_Button](/images/home/HOME_DEVICE_MNG_DELETE_DEVICE_01.PNG)button

1. Product Config
    Go to each engine by tabs and review your configuration
    ![Delete-Product_Config](/images/home/HOME_DEVICE_MNG_DELETE_DEVICE_02.PNG)

1. Environment Config (Service Config)
    Go to Service Config to review and click on the deployed environmenets
    ![Delete-Env_Config](/images/home/HOME_DEVICE_MNG_DELETE_DEVICE_03.PNG)

1. Finish config and Send Job.

    Now come back to the first selector and click on the finish config ( or cance if you don't want to send), Click finish again.

    ![Delete-Finish](/images/home/HOME_DEVICE_MNG_DELETE_DEVICE_04.PNG)

    you will see a spin lock while waiting for the job begins the execution.

    ![Delete-Spinner](/images/home/HOME_DEVICE_MNG_DELETE_DEVICE_05.PNG)

    When the job has been sent you will be redirected to the device list, where you will see your device in a pending state. ( Waiting to finish )

    ![Delete-List](/images/home/HOME_DEVICE_MNG_DELETE_DEVICE_06.PNG)

    You will be able to see the state and the result if you click on the state 

    ![Delete-Result_Caret](/images/home/HOME_DEVICE_MNG_DELETE_DEVICE_07.PNG)

    ![Delete-Result_Modal](/images/home/HOME_DEVICE_MNG_DELETE_DEVICE_08.PNG)