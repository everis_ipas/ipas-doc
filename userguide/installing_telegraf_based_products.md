# Installing Telegraf based products.

For every all telegraf based product , the product depens on the telegraf product. You should install first the telegraf product and then the dependent product.

When telegraf is intalled you will put on the remote server:

* __binaries__:  all executables needed 
* __Basic Conf__: a littel setup which self monitoring metrics (about used resources, metrics gathered , etc)

Rigth now (2019/06/03) these are the dependant products.

* __Linux__: will add needed config files to the telegraf conf dir to gather Linux SO  metrics
* __Windows__: will add needed config files to the telegraf conf dir to gather Linux Windows  metrics
* __Tomcat__: will add needed config files to the telegraf conf dir to gather Tomcat instances metrics
* __MySQL__: will add needed config files to the telegraf conf dir to gather mysql instances metrics
* __Apache__: will add needed config files to the telegraf conf dir to gather apache instances metrics


Remember  to check Linux and windows [system requirements](/installation/requirements.md)


## Installing Telegraf

Telegraf is the product we will need to gather data from several other sources , we will need at least to get metrics from other dependent products like Linux, tomcat, apache,  windows.

There is two posible config to choose 

 * Telegraf BASE Linux: will install base binaries for linux and get metrics from itself to the DB Engine
 * Telegraf BASE Windows: will install base binaries for windows and will send metrics from itself to the DB Engine

   ![New-Device](/images/home/products/TELEGRAF_INSTALL_00.png)
 

Once choosed you will need to configure some  basic parameters needed to install in the selected server.


Parameter| Meaning
-------|--------
Ansible Protocol | ssh if you will need to intall on linux boxes, winrm for windows ( self choosed depending on the used config)
Admin User | any administrative user system user: <ul><li>in a linux box use with sudo to root privileges</li><li>in the windows box with administrator privileges</li><ul>
Admin Password |  the password for the  previous defined administrative user.
Global Tags | Tags common to all telegraf based products ( should be the same as defined in the IPAS setup.conf!!!)


## Installing Linux

For Linux , there is only one available configuration, once choosed you will be prompted 

 * OS Linux: will install config files to get SO metrics

   ![New-Device](/images/home/products/LINUX_INSTALL_00.png)


Parameter| Meaning
-------|--------
Ansible Protocol | ssh  forced ( any other value will make FAIL the execution job)
Admin User | any administrative user system user with sudo to root privileges
Admin Password |  the password for the  previous defined administrative user.



## Installing Windows

As for Linux , there is only one available configuration, once choosed you will be prompted 

 * OS Windows: will install config files to get windows SO metrics

   ![New-Device](/images/home/products/WINDOWS_INSTALL_00.png)


Parameter| Meaning
-------|--------
Ansible Protocol | winrm  forced ( any other value will make FAIL the execution job)
Admin User | any administrator user 
Admin Password |  the password for the  previous defined administrator user.


## Installing Tomcat

Tomcat has only one available configuration, once choosed you will be prompted  for other paràmeters

 * AS Tomcat (Basic): will install config files to get windows SO metrics

   ![New-Device](/images/home/products/TOMCAT_INSTALL_00.png)


Parameter| Meaning
-------|--------
Ansible Protocol | winrm /ssh deppending on the target host os
System Admin User | any administrative user system user: <ul><li>in a linux box use with sudo to root privileges</li><li>in the windows box with administrator privileges</li><ul>
System Admin Password |  the password for the  previous defined administrative user.
Tomcat Admin User | tomcat user with admin api access  (for all instances)
Tomcat Admin Password |  tomcat password for the  previous defined user.
Tomcat Instances CSV | a CVS list or instance_name=URL,instance_name2=URL2 (by example)<br><br>instance1=http://localhost:8080,instance2=http://localhost:8081\"


## Installing Apache

There is two posible config to choose 

 * Apache BASIC [ mod_status and access.log parser ]: Will parse data from mod_status and LOGS with combined log format
 * Apache BASIC 2 [ mod_status and access.log with Response Time ]" Will parse data from mod_status and LOGS with combined log format and response time (%D)

   ![New-Device](/images/home/products/APACHE_INSTALL_00.png)




Parameter| Meaning
-------|--------
Ansible Protocol | winrm /ssh deppending on the target host os
System Admin User | any administrative user system user: <ul><li>in a linux box use with sudo to root privileges</li><li>in the windows box with administrator privileges</li><ul>
System Admin Password |  the password for the  previous defined administrative user.
Apache Basic Auth user for mod_status | apache basic HTTP based auth user   (for all instances)
Apache Basic Auth user for mod_status | apache basic HTTP based aith user password (for all instances)
Apache  mod_status CSV array | a CVS list or instance_name=URL1,instance_name2=URL2 (by example)<br><br>instance1=http://localhost:80/server-status,instance2=http://localhost:81/server-status
Apache Log file CSV array | a CVS list or instance_name=log_file_1,instance_name2=log_file_2 (by example)<br><br>instance1=/var/log/apache2/i1_access.log,instance2=/var/log/apache2/i2_access.log