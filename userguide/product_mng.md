# Product Management

## What is a Product ?

**Product** in the Metric HOME environment is refered to any kind of "metric group" or "service" that shares the same metrics.

You could manage your products from the Product section in the Navigator Bar

![Management Bar](/images/home/HOME_NAVIGATOR_BAR_PRODUCT_SELECTED_00.PNG)

Management devices includes

* [**Request new product**](#request-new-product) Request resources for a new product
* [**Modify a product**](#modify-product) Modify the configuration of a product

## Requeriments

In order to add a products you should previously tested in your laboratory environtment you can configure this device/instance for your desired product and all is working fine.

---

## Request new product

> You will need to be sure that used engines are already implemented

1. Click "Request new product" on the product management view.

    ![New-Device](/images/home/HOME_PRODUCT_MNG_NEW_PRODUCT_00.PNG)

1. On new product request, you will be asked to add the following information:

    * **Platform Requeriments** : 

    Paramter | Description | Normalizations  | Example
    ---------| ----------- | :----------------: | :--------:
    `Product Name` | Product name identifier | <ul><li> Must be uncapitalized</li><li>Can't contain spaces</li></ul> | <ul><li>`linux`</li><li>`bluecoat_av`</li></ul>
    `Model` | Models that the product is homologated to | -- | `RHEL 7.2, RHEL 7.4`
    `Description` | User friendly description of the product  | -- | `XXX`


    ![New-Device_Properties](/images/home/HOME_PRODUCT_MNG_NEW_PRODUCT_01.PNG)

1. Choose Gather Engines

    ![Gather_Engines](/images/home/HOME_PRODUCT_MNG_NEW_PRODUCT_02.PNG)

1. Choose Visual Engines

    ![Visual_Engines](/images/home/HOME_PRODUCT_MNG_NEW_PRODUCT_03.PNG)

1. Choose Alert Engines

    ![Alert_Engines](/images/home/HOME_PRODUCT_MNG_NEW_PRODUCT_04.PNG)

1. Finish config and Send Job.

    Now come back to the first selector and click on the finish config ( or cance if you don't want to send), Click finish again.

    ![New-Fininsh](/images/home/HOME_PRODUCT_MNG_NEW_PRODUCT_05.PNG)

    you will see a spin lock while waiting for the job begins the execution.

    ![New-Spinner](/images/home/HOME_PRODUCT_MNG_NEW_PRODUCT_06.PNG)

    When the job has been sent you will be redirected to the product list.

    ![New-List](/images/home/HOME_DEVICE_MNG_NEW_DEVICE_11.PNG)

    Your product must be validated by HOME admins. You won't be able to add any  configuration until:

    > Check product administration document to admin products

    * DB is created and assigned
    * Gather environments are assigned
    * Visual environments are assigned
    * Alert environments are assigned


---

## Modify product

> A user can modify a product but the engines assigned are not available to be modified yet

1. Search for the product you want to edit

    You can use the fiter input box to find your product. 
    ![Edit-List](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_00.PNG)

1. Click in modify button

    The available actions appear on `Action` column and allows the user to interact wich each product deployment. To modify the product, click on ![Edit-Edit_Button](/images/home/HOME_DEVICE_MNG_EDIT_DEVICE_01.PNG) button

1. Select what section you want to modify

    The available sections will appear on modal and the user must select the desired one. Note that only the sections with enviroment assigned are available to be modified.

    ![Edit-Product_Config](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_01.PNG)

1. Product properties (Service Config)

    On  the  first step you will be asked to review the model and description set on  `new product request`

    ![Edit-Prod_Config](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_02.PNG)


1. Section config

    On  the second step you will be able to see all  available engines for this product (already requested on)

    ![Edit-Section_Config](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_03.PNG)

    Click on each engine to fill your desired configurations

    ![Edit-Config_Mng](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_04.PNG)

1. Config management

    Click on ![Edit-Config_Mng](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_05.PNG) button to  add a new configuration.

    ### Product configuration

    Those are the different parameters to identify the configuration of the selected engine. You will be asked to fill the following params:    
    

    Parameter | Description | Normalization | Example|
    --------- | ------------|-------------|----------|
    `ID`  | id of the configuration | <ul><li>Must be uncapitalized</li><li>Must not contain blank spaces </li></ul>  | linux_base_001
    `Label` | friendly name of the confituration | -- | Basic linux configuration with cpu, memory, swap, disk and net metrics | Linux Basics
    `Models`| product models that are homologated for this configuration | -- | RHEL 6, RHEL 7

    ![Edit-Prod_Config](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_06.PNG)



    ### Configuration files

    The frontend allows to upload files for each engine/configuration. The following parameters will be asked

    Parameter | Description | Normalization | Example|
    ----| --- | --- | --- 
    `Filename`| Filename that will be uploaded into repository | <ul><li>Actually the filename is fixed with the upload one</li></ul> | <ul><li>`input_os_linux.conf`</li><li>`output_os_linux.conf`</li></ul>
    `Dest`| Destination path that will be used on the ansible playbook to store the file in | -- | `/etc/telegraf/telegraf.d/input_os_linux.conf`
    `File input` | Input that allows the user to upload a file | -- | <ul><li>`input_os_linux.conf`</li><li>`output_os_linux.conf`</li></ul>

    ![Edit-Prod_Config](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_07.PNG)

    ### Parameters

    In this section  and depending on the engine, the user will be able to fill and manage parameters based on thre types:

    Type | Description | Notes
    --- | --- | ---
    `Product Params` | Parameters that are directly from the product and will be common for all devices| **Not shown on device deployment**
    `Platform Params` | Parameters based on the environment and the platform and will be common for all devices | **Not shown on device deployment**
    `Device Params` | Parameters that will be different on each device | **Shown on device deployment**

    ![Edit-Prod_Config](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_08.PNG)

    On some engines parameters can be managed. A parameter is based on the following fields:

    Field | Description | Normalization | Example
    --- | --- | --- | ---
    `Key` | Parameter Key to be used internally by the engines | <ul><li>Must not contain blank spaces</li></ul> | <ul><li>ansible_user</li></ul>
    `Label` | Friendly name of the parameter that will be shown to  the user | -- | Ansible Username
    `Description` | Description of the parameter. The content will be shown on the `help` icon | Ansible username to be used on depoyments
    `Type` | Type of the parameter| Must be one of the following : <ul><li> array</li><li>boolean</li><li>string</li><li>integer</li> | `string`
    `Default value` | Default value to be used (or shown to the user)

    ![Edit-Param_Mng](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_09.PNG)

1. Finish config and Send Job.

    Now come back to the first selector and click on the finish config ( or cance if you don't want to send), Click finish again.

    ![Edit-Finish](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_10.PNG)

    you will see a spin lock while waiting for the job begins the execution.

    ![Edit-Spinner](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_11.PNG)

    When the job has been sent you will be redirected to the product list

    ![Edit-List](/images/home/HOME_PRODUCT_MNG_EDIT_PRODUCT_12.PNG)
