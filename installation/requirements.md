## IPAS Agents Requirements

## Products based on Snmpcollector Gather Engine.

All target devices will need for SNMP incomming request from the IPAS server , also needed ACL's to query the complete MIB tree for the device.

## Products based on Telegraf Gather Engine.

All target servers will need for TCP/IP connectivity with our central server

Product |  Monitored Infraestructure Requirements
-- | -- 
Linux |  Agent will be installed with ansible, linux targets needs for python 2 (version  2.6 or later )  for python 3 ( version3.5 or later), libselinux-python ( on those targets with selinux installed). Instalation will be done with an admin user (not root) but with sudo access to root.
Windows | For Ansible to communicate to a Windows host and use Windows modules, the Windows host must meet the following requirements:<ul><li>Ansible’s supported Windows versions generally match those under current and extended support from Microsoft. Supported desktop OSs include Windows 7, 8.1, and 10, and supported server OSs are Windows Server 2008, 2008 R2, 2012, 2012 R2, 2016, and 2019.</li><li>Ansible requires PowerShell 3.0 or newer and at least .NET 4.0 to be installed on the Windows host.</li><li>A WinRM listener should be created and activated. More details for this can be found below.</li></ul><ul>Follow these steps to config your windows host https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html , on recent releases you will only should to execute  a simple script [ConfigureRemotingForAnsible.ps1](https://github.com/ansible/ansible/blob/devel/examples/scripts/ConfigureRemotingForAnsible.ps1)  <ul>
Apache | Requires for each apache instance: <ul><li> mod_status  configured with:<ul><li> ExtendedStatus=On</li><li>  URL = http://localhost:XXX/server-status</li><li>  User  = apache_viewer (basic auth user) </li><li>  Passwd = apache123 (basic auth passwd)</li></ul></li><li>If you will need gather response times you also will need: <ul><li>Access log configured in Apache tipical COMBINED_LOG_FORMAT adding %D (response time, as last parameter in the configuration)<br/>_LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\" %D" combined_</li></ul></li></ul>
MsIIS |  (Pending)
nginx |   (Pending) 
Oracle |   Connection data host, port, dbname, db_user, db_password 
Tomcat |  Connection data to its REST API from localhost  Example:     <ul><li> url = "http://127.0.0.1:8080/manager/status/all?XML=true"</li><li> username = "tomcat"  (HTTP Basic Creentials)</li><li> password = "s3cret"</li></ul>
mysql |   Connection data     [username[:password]@][protocol[(address)]]/[?tls=[true\|false\|skip-verify]]       Example:       user:passwd@tcp(127.0.0.1:3306)/?tls=false 
PostgresSQL |   Connection data :   User, Password, hostname,   dbname, sslmode   Example:   postgres://[pqgotest[:password]]@localhost[/dbname]?sslmode=[disable\|verify-ca\|verify-full] 
SQLServer | We will need a connection user for each instance we need monitoring and also connection data    (hostname , port)        ( setup example  in one instance)       USE master;GOCREATE LOGIN [telegraf] WITH PASSWORD = N'mystrongpassword';GOGRANT VIEW SERVER STATE TO [telegraf];GOGRANT VIEW ANY DEFINITION TO [telegraf];GO 



