# IPAS Comunications

IPAS needs comunicate with its environment in several ways and with different communication protocols depending on the context.


![Comunication diagram](/images/diagrams/ipas_networking.png)


## USER ACCESS 

Users will connect to IPAS usually with a browser using HTTP/HTTPS (TCP/80 and TCP/443), conection could be done througth proxy servers.

## GATHERING DATA FROM DEVICES

### Networking Devices

Networking devices will be polled thougth SNMP protocol (usually and recomended UDP 161).

### Linux Servers

IPAS will install an agent witch will be responsible to send data  from the remote server into the IPAS Database through HTTP protocol over 8086 or HTTPS over 8486

The agent will be installed  from the IPAS Server to all generic Servers with over SSH protocol (TCP/22)

### Windows Servers

IPAS will install an agent witch will be responsible to send data from the remote server into the IPAS Database through HTTP protocol over 8086 or HTTPS over 8486

The agent will be installed  from the IPAS Server to all windows Servers over WINRM(HTTP) (TCP/5986)

### AIX/HMC 

IPAS will poll data from the HMC Server over the HTTPS protocol on 12443 port, could also retrieve data from nmon files over the SSH/SFTP protocol on port 22

### VSphere VCenter

IPAS will poll data from the VMWare VSphere System over HTTPS protocol (usually on port 443) IT depens on the VCenter configuration.


## IPAS Conection with the WORLD

IPAS needs comunicate with several public services to download sofware. these are some of these url's , but could change on future releases

* http(s)://*dockerproject.org
* http(s)://*docker.io
* http(s)://*.hub.docker.com
* http(s)://dl.google.com
* http(s)://*.jenkins.io
* http(s)://*.python.org
* http(s)://pypi.org
* http(s)://pypi.python.org
* http(s)://*.npmjs.org
* http(s)://*.nodejs.org
* http(s)//*bitbucket.org
* http(s)://*golang.org
* http(s)://*gopkg.in
* http(s)://*.grafana.com
* http(s)://*.debian.org
* http(s)://*.osuosl.org
* http(s)://*.jenkins-ci.org
* http(s)://*.pythonhosted.org
* http(s)://*.github.com
* http(s)://*.githubusercontent.com
* http(s)://*s3.amazonaws.com
* http(s)://*.aws.dckr.io
* http(s)://*.elb.amazonaws.com
