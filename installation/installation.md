# IPAS-docker

IPAS- its a  powerful , scalable  and customized platform for infrastructure and business components through  performance data gathering, storage, real time visualization and monitoring.
Can be implemented as a complement for other monitoring systems or as an standalone platform to get metrics and monitoring.
Includes a powerful database  for events and time series data, a set of predefined key metrics templates for common software and hardware , and also predefined rules for both rule based monitoring and machine learning for advanced Anomaly detection in IT  infrastructures

## Platform Requirements

### Software Requirements:

#### Base software


* Linux ( Linux Kernel with storage overlay2 driver support ) (https://docs.docker.com/storage/storagedriver/overlayfs-driver/)
* docker-ce/ee ( https://docs.docker.com/install/)

   | Docker version         |	Kernel Version     |
   |------------------------|--------------------|
   |docker-ee (Centos/RHEL) | >=3.10.0-514       |
   |docker-ce (all)         | >=4.0              |

 * docker-compose (https://docs.docker.com/compose/install/)

***once installed , Be sure you can execute docker commands as the user (not root user) you will use to install IPAS (https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user)***

You can usually do that with these commands (as root)

````bash
sudo groupadd docker
sudo usermod -aG docker $USER
````
* Docker log rotation  config 

set the following content /etc/docker/daemon.json
```
{
  "log-driver": "json-file",
  "log-opts": {"max-size": "20m", "max-file": "5"}
}
```
Make sure to run a systemctl reload docker after changing this file to have the settings applied.

#### Tools (all as installed as root)

 * Git:  needed to download the config and install repos
 * curl: needed by the setup.sh script 
 * python: neede by the setup.sh script
 * Golang Runtime: as a runtime execution for the provisioning scripts (https://golang.org/doc/install)
 * Golang Yaml library  ( as non IPAS user)
````bash 
#go get gopkg.in/yaml.v2
````
 * ifconfig  command ( often in net-tools package)
 * ssh server up and running and ssh-keygen tool
 * jq tool for JSON data hanling in shell scripts
 * sqlite3 tool for inserting data in the home db

### Iptables Rules

IPAS needs for SSH and HTTP/HTTPS incomming connections from jenkins docker  container to the docker host(the IPAS server). 
When Docker works with firewalld ( Centos/Redhat ??)
So you should be aware that docker allows them issually with the IN_public_allow IPTABLES Chain (docker 17.12.1-ce).

#### Firewalld

````bash
# iptables -L IN_public_allow
Chain IN_public_allow (1 references)
target     prot opt source               destination
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:ssh ctstate NEW
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:https ctstate NEW
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:http ctstate NEW
````

if these 2 rules are not present we should add them with the following commands (as root)

````bash
firewall-cmd --permanent --zone=public --add-port=80/tcp
firewall-cmd --permanent --zone=public --add-port=443/tcp
firewall-cmd --permanent --zone=public --add-port=22/tcp
````

Is like adding these 2 iptables rules.

````
iptables -A IN_public_allow -p tcp --dport https  -m state --state NEW -j ACCEPT
iptables -A IN_public_allow -p tcp --dport http  -m state --state NEW -j ACCEPT
iptables -A IN_public_allow -p tcp --dport ssh  -m state --state NEW -j ACCEPT
````
#### Raw IPTables 

You can add in /etc/sysconfig/iptables  these 3 rules before the  (-A INPUT -j REJECT --reject-with icmp-host-prohibited)

````
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
````


### Hardware  Requirements:
* CPU: > 4 cpu's  y 
* RAM: > 8 Gb RAM
* Storage: 400 Gb  (SSD better than HDD) 
* Be sure you have previous storage mounted where data will be stored $USER_HOME/ipas-docker (before git clone) or $USER_HOME/ipas-docker/influxdb/data (after git clone) before the setup.sh execution.

### Other requirements.

 * TCP/IP visibility (direct or thougth firewall)  with all our distributed agents.
 * Intenet Access to common  software repositories ( direct or thougth Proxy )  -- bitbucket, hub.docker.com , other.. --
 * Just One DNS request for new A or CNAME for all our subdomains.

 Exampe:
 
 | Zone DNS |	Type  |	TTL	| Priority |	Value	|
 |----------|---------|-----|------------|----------|
 |*.ipas.mydomain.org |	CNAME |	- | - |	serverXX.mydomain.org |
 |*.ipas.mydomain.org |	A |	- | - |	W.X.Y.Z |

### Dns Server Config

IPAS server needs for a DNS server configured which could resolve all our *.ipas.mydomain.org  delegated subdomains,  this is, never use external (non corporative) dns servers.

### If you have to install thougth proxy

remember first to setup proxy configuration for all tools.

| Tool | Proxy Config |
|------|--------------|
| wget | export http_proxy=http://proxyuser:proxypwd@proxy.server.com:8080 |
| git  | git config --global http.proxy http://proxyuser:proxypwd@proxy.server.com:8080 |
| docker daemon | https://docs.docker.com/config/daemon/systemd/#httphttps-proxy |

If proxy needed , be sure you have set in your environment the following variables.( you can set in  /etc/profile.d/proxy.sh), take this one as an example, and be sure you have no_proxy set at least with 127.0.0.1 and localhost

````
# cat /etc/profile.d/proxy.sh
export http_proxy="htttp://myproxy:8080"
export https_proxy="http://myproxy:8080/"
export ftp_proxy="http://myproxy:8080/"
export no_proxy="127.0.0.1,localhost"
````

## Install Process

We should have a NO ROOT access, assume "ipasuser" as non admin system user. 
 
We should execute the following   el directorio HOME  de ipasuser /home/ipasuser

Be sure you have UMASK 0022  ( you can set into the ~/.basrc file)

````bash
git clone https://<userid>@bitbucket.org/everis_ipas/ipas-gva.git
git clone https://<userid>@bitbucket.org/everis_ipas/ipas-docker.git
cd ipas-docker
````

## Main setup.conf File

The setup process needs for some base config about TAGS for product, so you should create and edit the setup.conf file (from sample).

````bash
cp setup.conf.sample setup.conf
vim setup.conf
````

Setup.conf file have 8 columns 

* **Product Name/ID**: the same here than the product directoy name in the IPAS-GVA repo.
* **Influx Server URL**: where database will be created, if void  http://localhost:8086 will be set.
* **Database Name**: the database where this product data will be sent ( could be shared with other products, if repeated,next two parameters should be also repeated)
* **Database Retention**: the amount of time data will be retained in the database in dais.
* **Database Shard duration**: how many time bit of data will be splited (usualy the time wich you ususari will do queries , by example 24h if you most of time will show metrics form the las day)
* **Database RW username**: the user to write data into de db, if not set the username in the IPAS_DB_RW_USER will be set.
* **Database RW password**: the password to write data into de db, if not set the password in the IPAS_DB_RW_PASSORD will be set
* **Product tags**: a list of tag names for each product.

##  Add Other Services with the extra_sites.conf File

````bash
cp extra_services.conf.sample extra_services.conf
vim extra_services.conf
````

extra_services.conf file have 9 columns 

* **1.- svc_name**          :Public site under IPAS domain => <publi_name>.<IPAS_DOMAIN>
* **2.- svc_protocol**      :HTTP protocol (http/https)
* **3.- svc_tcpip_dir**     :real IP:PORT where http/https is listening
* **4.- svc_adm_user**      :Admin user
* **5.- svc_adm_pass**      :Admin Pass
* **6.- svc_status_uri**    :Status URI (example /login)
* **7.- svc_status_codes**  :Status CODES (as and array 200 by default)
* **8.- svc_label**         :Home Label to show for this service
* **9.- scv_desc**          :Home Description to show for this service

* **service_name**: Public site under IPAS domain, the FQDN will be service_name.${IPAS_DOMAIN_BASE}
* **Real Service Destination**: real IP:PORT where the extenal http/https will be listening


Before run the script you need the IPAS_DOMAIN_BASE variable ( your net domain which you did the DNS  request )

Also you shoudt set as Environtment variables the following.
* __IPAS_ADMIN_USER/PASSWORD:__ will become the admin access user and password for all our tools.
* __IPAS_DB_RD_USER/PASSWORD:__ will be the user and password to query (read) data from all databases on our influxdb.
* __IPAS_DB_RW_USER/PASSWORD:__ will be the user and password agents will use to send (and write) data to our influxdb

````bash
# User and password variables
export IPAS_ADMIN_USER="myadmin_user"                 #required
export IPAS_ADMIN_PASSWORD="myadmin_passwd"           #required
export IPAS_DB_RD_USER="db_read_user"                 #required
export IPAS_DB_RD_PASSWORD="db_read_password"         #required
export IPAS_DB_RW_USER="db_read_write_user"           #required
export IPAS_DB_RW_PASSWORD="db_read_write_password"   #required
# Domain Name variable
export IPAS_DOMAIN_BASE="mydomain.org"                #required
# Git Repo sources REPO CLONE WILL BE SET AS ${IPAS_REPO_PATH}/${REPO_NAME}
export IPAS_REPO_PATH="https://ipasdevel:1p4sm0l4@bitbucket.org/everis_ipas" #required
export IPAS_REPO_MIRRORED=false                       #required (false is you don't know what this do)
export IPAS_REPO_BRANCH="master"                      #required
# Certificate variables
export IPAS_CERT_COUNTRY="ES"           #default value if not set "ES"
export IPAS_CERT_ORG="MySuperORG"       #default value if not set "myORG"
export IPAS_CERT_CITY="Barcelona"       #default value if not set "Barcelona"
export IPAS_CERT_OU="IPAS"              #default value if not set "IPAS"
export IPAS_CERT_DURATION_DAYS="365"    #default value fi noet set "365"

./setup.sh -i

````
NOTE:  for base sofware restrictions and also by common sense the IPAS_ADMIN_USER variable can not be  set as "admin" 

The Instalation process does the following actions

* Config some useful aliases to help IPAS administration.
    * **icli**: influxdb client
    * **gcli**: grafana client
    * **kcli**: kapacitor client
    * **insh**: influx container shell
    * **grsh**: grafana container shell
    * **kash**: kapacitor container shell
    * **gish**: gitea container shell
    * **jesh**: jenkins container shell
    * **adesh**: Machine Learning Anomali detection engine shell
    * **nresh**: Machine Learning Noise Reduction Engine Shell
    * **docsh**: MKDoc container Shell
    * **proxy_conf**:  show nginx reverse proxy configuration for all the containers
    
* Set current user ready to access the docker engine 

* Start the following services on top of the docker engine.

    * **influxdb**:  BBDD influx
    * **Kapacitor**: Kapacitor
    * **grafana**:  Grafana
    * **telegraf**: Local agent that enables gather performance metrics for our own IPAS platform.
    * **snmpcollector**: Centralized Agent snmpcollector for SNMP based data gathering.
    * **sqlcollector**: Centralized Agent Sqlcollector to get SQL based data performance metrics
    * **resistor**: The main interface for Kapacitor Alert administration from a product based point of view
    * **resistor UDF injector (resinjector)**: And resitor based UDF kapacitor function to inject data from the resistor  
    * **jenkins**: The main tool to automate the "self-service" based IPAS platform 
    * **nginx_proxy**: Element that acts as a proxy for * .ipas.mydomain.org

* Creates all Influx Databases listen in the following file [ipas.db.txt]
* Creates all needed datasources for accessing to each database for each one of the previousy created databases.
* Scan the IPAS-gva repo (previouly downloaded ) for products Dashboards (visual.yaml) and regenerates them with the taglist templating in the setup.conf file.

## Validation

In order to play with IPAS you will need some environtment variables that will be loaded on new shells, on current you can load them by executing the following command.

````bash
source ~/.ipas_profile
````

Be sure you have DNS resolution for all IPAS services in the SUBDOMAIN (grafana.${DOMAIN},snmpcollector.${DOMAIN, etc.})  otherwise you can force in your local box by executing the following command

````bash
(as root user)
#cat /tmp/hosts >> /etc/hosts
````

You can test connecting to the following sites ( from anywhere if previously configured de DNS or from lo localmachine if not).

* http://www.${IPAS_DOMAIN_BASE} 

In the home you will be able to navigate through  all the other IPAS components.

* http://grafana.${IPAS_DOMAIN_BASE}
* http://snmpcollector.${IPAS_DOMAIN_BASE}
* http://resistor.${IPAS_DOMAIN_BASE}
* http://jenkins.${IPAS_DOMAIN_BASE}
* http://git.${IPAS_DOMAIN_BASE}
* http://doc.${IPAS_DOMAIN_BASE}


