# Infraestructure Performance Analytics System

IPAS its a powerful, scalable and customized platform for infrastructure and business components through  performance data gathering, storage, real time visualization and monitoring.
Can be implemented as a complement for other monitoring systems or as an standalone platform to get metrics and monitoring.

Includes a powerful database for events and time series data, a set of predefined key metrics templates for common software and hardware, and also predefined rules for both rule based monitoring and machine learning for advanced Anomaly detection in IT infrastructures.

You can test connecting to the following sites ( from anywhere if previously configured de DNS or from lo localmachine if not).

* http://grafana.${DOMAIN}
* http://snmpcollector.${DOMAIN}
* http://resistor.${DOMAIN}
* http://jenkins.${DOMAIN}